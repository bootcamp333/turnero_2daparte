from django.db import models

# Create your models here.
class Tipo_servicio(models.Model):
    servicio_desc = models.CharField(max_length=50, null=False, blank=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.servicio_desc

    class Meta:
        db_table = 'Tipo_servicio'

class Tipo_discapacidad(models.Model):
    discapacidad_desc = models.CharField(max_length=50, null=False, blank=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)
    
    def __str__(self) -> str:
        return self.discapacidad_desc

    class Meta:
        db_table = 'Tipo_discapacidad'

class Tipo_cliente(models.Model):
    tipo_cliente_id = models.AutoField(primary_key=True)
    tipo_cliente_desc = models.CharField(max_length=50, null=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.tipo_cliente_desc

    class Meta:
        db_table = 'Tipo_cliente'

class Prioridades(models.Model):
    prioridad_id = models.AutoField(primary_key=True)
    prioridad_desc = models.CharField(max_length=50, null=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.prioridad_desc

    class Meta:
        db_table = 'Prioridades'

class Nacionalidad(models.Model):
    nacionalidad_id = models.AutoField(primary_key=True)
    nacionalidad_desc = models.CharField(max_length=50, null=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.nacionalidad_desc

    class Meta:
        db_table = 'Nacionalidad'

class Ciudad(models.Model):
    ciudad_id = models.AutoField(primary_key=True)
    ciudad_desc = models.CharField(max_length=50, null=False)
    nacionalidad_id = models.ForeignKey(Nacionalidad, on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.ciudad_desc

    class Meta:
        db_table = 'Ciudad'

class Estado(models.Model):
    estado_id = models.AutoField(primary_key=True)
    estado_desc = models.CharField(max_length=50, null=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.estado_desc

    class Meta:
        db_table = 'Estado'

class Persona(models.Model):
    persona_id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False)
    apellido = models.CharField(max_length=50, null=False)
    cedula = models.IntegerField(null=False)
    telefono = models.IntegerField(null=True)
    calle = models.CharField(max_length=100, null=True)
    email = models.CharField(max_length=50, null=True)
    fecha_nacimiento = models.DateTimeField(null=False)
    discapacidad_id = models.ForeignKey(Tipo_discapacidad, on_delete=models.CASCADE)
    ciudad_id = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self):
        return (self.nombre +" "+ self.apellido)

    class Meta:
        db_table = 'Persona'

class Cliente(models.Model):
    cliente_id = models.AutoField(primary_key=True)
    cliente_nro = models.IntegerField(null=False)
    razon_social = models.CharField(max_length=50, null=True)
    ruc = models.IntegerField(null=False)
    persona_id = models.ForeignKey(Persona, on_delete=models.CASCADE)
    nacionalidad_id = models.ForeignKey(Nacionalidad, on_delete=models.CASCADE)
    tipo_cliente_id = models.ForeignKey(Tipo_cliente, on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def IntegerRangeField(self):
        return self.persona_id
    def __str__(self) -> str:
        return self.razon_social

    class Meta:
        db_table = 'Cliente'

class Puesto(models.Model):
    puesto_id = models.AutoField(primary_key=True)
    puesto_desc = models.CharField(max_length=50, null=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.puesto_desc

    class Meta:
        db_table = 'Puesto'

class Trabajador(models.Model):
    trabajador_id = models.AutoField(primary_key=True)
    persona_id = models.ForeignKey(Persona, on_delete=models.CASCADE)
    nacionalidad_id = models.ForeignKey(Nacionalidad, on_delete=models.CASCADE)
    puesto_id = models.ForeignKey(Puesto, on_delete=models.CASCADE)
    fecha_inicio = models.DateTimeField(null=False)
    fecha_fin = models.DateTimeField(null=True)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def IntegerRangeField(self):
        return self.persona_id

    class Meta:
        db_table = 'Trabajador'

class Rol(models.Model):
    rol_id = models.AutoField(primary_key=True)
    rol_desc = models.CharField(max_length=50, null=False)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.rol_desc

    class Meta:
        db_table = 'Rol'

class Ticket(models.Model):
    ticket_id = models.AutoField(primary_key=True)
    ticket_desc = models.CharField(max_length=50, null=False)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)
    cliente_id = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    prioridad_id = models.ForeignKey(Prioridades, on_delete=models.CASCADE)
    servicio_id = models.ForeignKey(Tipo_servicio, on_delete=models.CASCADE)
    puesto_id = models.ForeignKey(Puesto, on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.ticket_desc

    class Meta:
        db_table = 'Ticket'

class Usuario(models.Model):
    usuario_id = models.AutoField(primary_key=True)
    usuario_alias = models.CharField(max_length=50, null=False)
    contrasena = models.CharField(max_length=50, null=False, blank=False)
    trabajador_id = models.ForeignKey(Trabajador, on_delete=models.CASCADE)
    rol_id = models.ForeignKey(Rol, on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.usuario_alias

    class Meta:
        db_table = 'Usuario'

class Cola_espera(models.Model):
    cola_id = models.AutoField(primary_key=True)
    cola_desc = models.CharField(max_length=50, null=False)
    ticket_id = models.ForeignKey(Ticket, on_delete=models.CASCADE)
    trabajador_id = models.ForeignKey(Trabajador, on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField(null=True)
    usuario_insercion = models.CharField(max_length=50, null=True)
    fecha_modificacion = models.DateTimeField(null=True)
    usuario_modificacion = models.CharField(max_length=50, null=True)

    def __str__(self) -> str:
        return self.cola_desc

    class Meta:
        db_table = 'Cola_espera'