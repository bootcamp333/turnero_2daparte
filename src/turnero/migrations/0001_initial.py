# Generated by Django 4.1.6 on 2023-02-14 01:51

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tipo_servicio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('servicio_id', models.BigIntegerField()),
                ('servicio_desc', models.CharField(max_length=50)),
                ('usuario_mod', models.CharField(max_length=50)),
            ],
        ),
    ]
