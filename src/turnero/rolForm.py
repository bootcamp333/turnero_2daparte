from django import forms

class RolFormulario(forms.Form):
    rol_descripcion = forms.CharField(max_length=50, required=True)
