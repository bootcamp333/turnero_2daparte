from django import forms

class PersonaFormulario(forms.Form):
    nombre = forms.CharField(max_length=50, required=False)
    apellido = forms.CharField(max_length=50, required=False)
    cedula = forms.IntegerField(required=False)
    telefono = forms.IntegerField(required=True)
    calle = forms.CharField(max_length=100, required=True)
    email = forms.CharField(max_length=50, required=True)
    fecha_nacimiento = forms.DateTimeField(required=False)
    fecha_insercion = forms.DateTimeField(required=True)
    usuario_insercion = forms.CharField(max_length=50, required=True)
    fecha_modificacion = forms.DateTimeField(required=True)
    usuario_modificacion = forms.CharField(max_length=50, required=True)