from django import forms
from .models import Rol, Persona, Cliente, Cola_espera, Nacionalidad, Tipo_servicio, Tipo_cliente, Trabajador, Puesto, Ciudad, Estado, Ticket, Prioridades, Tipo_discapacidad
from django.contrib.auth import password_validation

class RolModelForm(forms.ModelForm):
    class Meta:
        model = Rol
        fields = ["rol_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]
        
class PersonaModelForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = ["nombre","apellido","cedula", "telefono", "calle", "email", "fecha_nacimiento", "discapacidad_id", "ciudad_id", "fecha_insercion", "usuario_insercion","fecha_modificacion","usuario_modificacion"]

class ClienteModelForm(forms.ModelForm):
    class Meta:
        model = Cliente
        fields = ["cliente_nro","razon_social","ruc", "persona_id", "persona_id", "nacionalidad_id", "tipo_cliente_id", "fecha_insercion", "usuario_insercion","fecha_modificacion","usuario_modificacion"]

class NacionalidadModelForm(forms.ModelForm):
    class Meta:
        model = Nacionalidad
        fields = ["nacionalidad_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class TipoClienteModelForm(forms.ModelForm):
    class Meta:
        model = Tipo_cliente
        fields = ["tipo_cliente_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class TrabajadorModelForm(forms.ModelForm):
    class Meta:
        model = Trabajador
        fields = ["persona_id","nacionalidad_id","puesto_id","fecha_inicio","fecha_fin","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class PuestoModelForm(forms.ModelForm):
    class Meta:
        model = Puesto
        fields = ["puesto_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class CiudadModelForm(forms.ModelForm):
    class Meta:
        model = Ciudad
        fields = ["ciudad_desc","nacionalidad_id","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class EstadoModelForm(forms.ModelForm):
    class Meta:
        model = Estado
        fields = ["estado_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class TicketModelForm(forms.ModelForm):
    class Meta:
        model = Ticket
        fields = ["ticket_desc","estado","cliente_id","prioridad_id","servicio_id","puesto_id","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class PrioridadModelForm(forms.ModelForm):
    class Meta:
        model = Prioridades
        fields = ["prioridad_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class DiscapacidadModelForm(forms.ModelForm):
    class Meta:
        model = Tipo_discapacidad
        fields = ["discapacidad_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"] 

class ServicioModelForm(forms.ModelForm):
    class Meta:
        model = Tipo_servicio
        fields = ["servicio_desc","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"]

class ColaModelForm(forms.ModelForm):
    class Meta:
        model = Cola_espera
        fields = ["cola_desc","ticket_id","fecha_insercion","usuario_insercion","fecha_modificacion","usuario_modificacion"] 
