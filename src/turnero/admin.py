from django.contrib import admin
from . import models
from .forms import RolModelForm

# Register your models here.

class RolAdmin(admin.ModelAdmin):
    list_display = ["rol_desc", "fecha_insercion"]
    list_filter = ["fecha_modificacion"]
    list_editable = []
    search_fields = ["rol_desc"]
    form = RolModelForm

class PersonaAdmin(admin.ModelAdmin):
    list_display = ["nombre", "apellido", "cedula"]
    list_filter = ["cedula"]
    list_editable = []
    search_fields = ["apellido", "cedula"]
    class Meta:
        model = models.Persona

class CiudadAdmin(admin.ModelAdmin):
    list_display = ["ciudad_id", "ciudad_desc", "nacionalidad_id"]
    list_filter = ["ciudad_desc"]
    list_editable = ["ciudad_desc"]
    search_fields = ["ciudad_desc"]

class ClienteAdmin(admin.ModelAdmin):
    list_display = ["cliente_id", "cliente_nro", "persona_id", "tipo_cliente_id", "razon_social", "nacionalidad_id"]
    list_filter = ["cliente_nro"]
    list_editable = ["persona_id", "tipo_cliente_id", "cliente_nro"]
    search_fields = ["cliente_nro"]

class TrabajadorAdmin(admin.ModelAdmin):
    list_display = ["nacionalidad_id", "puesto_id", "fecha_inicio", "fecha_fin", "persona_id"]
    list_filter = ["persona_id"]
    list_editable = ["puesto_id", "persona_id", "fecha_fin"]
    search_fields = ["puesto_id"]

class EstadoAdmin(admin.ModelAdmin):
    list_display = ["estado_desc", "estado_id"]
    list_filter = ["estado_desc"]
    list_editable = []
    search_fields = []

admin.site.register(models.Rol, RolAdmin)
admin.site.register(models.Ciudad, CiudadAdmin)
admin.site.register(models.Nacionalidad)
admin.site.register(models.Persona, PersonaAdmin)
admin.site.register(models.Tipo_discapacidad)
admin.site.register(models.Cliente, ClienteAdmin)
admin.site.register(models.Tipo_servicio)
admin.site.register(models.Tipo_cliente)
admin.site.register(models.Prioridades)
admin.site.register(models.Estado, EstadoAdmin)
admin.site.register(models.Puesto)
admin.site.register(models.Trabajador, TrabajadorAdmin)
admin.site.register(models.Ticket)
admin.site.register(models.Usuario)
admin.site.register(models.Cola_espera)
