
from django.contrib import messages
from django.forms import model_to_dict
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from .models import Rol, Persona, Cliente, Nacionalidad, Cola_espera, Tipo_servicio, Tipo_cliente, Trabajador, Puesto, Ciudad, Estado, Ticket, Prioridades, Tipo_discapacidad
from .forms import RolModelForm, PersonaModelForm, ClienteModelForm, ColaModelForm, ServicioModelForm, NacionalidadModelForm, TipoClienteModelForm, TrabajadorModelForm, PuestoModelForm, CiudadModelForm, EstadoModelForm, TicketModelForm, PrioridadModelForm, DiscapacidadModelForm
# Create your views here.
from django.conf import settings
def Inicio(request):
    print(settings.BASE_DIR)
    context = {
        "titulo":'Formulario'
    }
    return render(request, "inicio.html",context)

def roles(request):
    num_rol=Rol.objects.all().count()
    print(settings.BASE_DIR)
    form = RolModelForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo":'Formulario de Roles',
        "el_form":form,
        "num_rol":num_rol
    }
    return render(request, "roles.html",context)

def Personavista(request):
    num_rol1=Persona.objects.all().count()
    print(settings.BASE_DIR)
    form1 = PersonaModelForm(request.POST or None)
    if form1.is_valid():
        instance = form1.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo1":'Formulario de Personas',
        "el_form1":form1,
        "num_rol1":num_rol1
    }
    return render(request, "personas.html",context)

def Clientevista(request):
    num_rol2=Cliente.objects.all().count()
    priori=Prioridades.objects.filter(prioridad_desc='ALTA')
    print(settings.BASE_DIR)
    form2 = ClienteModelForm(request.POST or None)
    if form2.is_valid():
        instance = form2.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo2":'Formulario de Clientes',
        "el_form2":form2,
        "num_rol2":num_rol2,
        "priori":priori
    }
    return render(request, "clientes.html",context)

def Nacionalidadvista(request):
    num_rol3=Nacionalidad.objects.all().count()
    print(settings.BASE_DIR)
    form3 = NacionalidadModelForm(request.POST or None)
    if form3.is_valid():
        instance = form3.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo3":'Formulario de Nacionalidades',
        "el_form3":form3,
        "num_rol3":num_rol3
    }
    return render(request, "nacionalidad.html",context)

def TipoClientevista(request):
    num_rol4=Tipo_cliente.objects.all().count()
    print(settings.BASE_DIR)
    form4 = TipoClienteModelForm(request.POST or None)
    if form4.is_valid():
        instance = form4.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo4":'Formulario Tipo Clientes',
        "el_form4":form4,
        "num_rol4":num_rol4
    }
    return render(request, "tipoCliente.html",context)

def Trabajadorvista(request):
    num_rol5=Trabajador.objects.all().count()
    print(settings.BASE_DIR)
    form5 = TrabajadorModelForm(request.POST or None)
    if form5.is_valid():
        instance = form5.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo5":'Formulario Funcionarios',
        "el_form5":form5,
        "num_rol5":num_rol5
    }
    return render(request, "trabajador.html",context)

def Puestovista(request):
    num_rol6=Puesto.objects.all().count()
    print(settings.BASE_DIR)
    form6 = PuestoModelForm(request.POST or None)
    if form6.is_valid():
        instance = form6.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo6":'Formulario de Puestos',
        "el_form6":form6,
        "num_rol6":num_rol6
    }
    return render(request, "puesto.html",context)

def Ciudadvista(request):
    num_rol7=Ciudad.objects.all().count()
    print(settings.BASE_DIR)
    form7 = CiudadModelForm(request.POST or None)
    if form7.is_valid():
        instance = form7.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo7":'Formulario de Ciudades',
        "el_form7":form7,
        "num_rol7":num_rol7
    }
    return render(request, "ciudad.html",context)

def Estadovista(request):
    num_rol8=Estado.objects.all().count()
    print(settings.BASE_DIR)
    form8 = EstadoModelForm(request.POST or None)
    if form8.is_valid():
        instance = form8.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo8":'Formulario de Estado',
        "el_form8":form8,
        "num_rol8":num_rol8
    }
    return render(request, "estado.html",context)

def Ticketvista(request):
    num_rol9=Ticket.objects.all().count()
    print(settings.BASE_DIR)
    form9 = TicketModelForm(request.POST or None)
    if form9.is_valid():
        instance = form9.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo9":'Formulario de Tickets',
        "el_form9":form9,
        "num_rol9":num_rol9
    }
    return render(request, "ticket.html",context)

def Prioridadvista(request):
    num_rol10=Prioridades.objects.all().count()
    print(settings.BASE_DIR)
    form10 = PrioridadModelForm(request.POST or None)
    if form10.is_valid():
        instance = form10.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo10":'Formulario de Prioridades',
        "el_form10":form10,
        "num_rol10":num_rol10
    }
    return render(request, "prioridad.html",context)

def Discapacidadvista(request):
    num_rol11=Tipo_discapacidad.objects.all().count()
    print(settings.BASE_DIR)
    form11 = DiscapacidadModelForm(request.POST or None)
    if form11.is_valid():
        instance = form11.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo11":'Formulario de Discapacidades',
        "el_form11":form11,
        "num_rol11":num_rol11
    }
    return render(request, "discapacidad.html",context)

def Serviciovista(request):
    num_rol12=Tipo_servicio.objects.all().count()
    print(settings.BASE_DIR)
    form12 = ServicioModelForm(request.POST or None)
    if form12.is_valid():
        instance = form12.save(commit=False)
        instance.save()
        print(instance)
        messages.success(request, "Guardado exitoso")
    else:
        messages.error(request, "No se pudo guardar")
    context = {
        "titulo12":'Formulario de Servicios',
        "el_form12":form12,
        "num_rol12":num_rol12
    }
    return render(request, "servicios.html",context)

def Colavista(request):
    if request.user.is_authenticated:
        num_rol13=Cola_espera.objects.all().count()
        pending=Ticket.objects.filter(estado='6').order_by('-fecha_modificacion')[:5]
        print(settings.BASE_DIR)
        form13 = ColaModelForm(request.POST or None)
        if form13.is_valid():
            instance = form13.save(commit=False)
            instance.save()
            print(instance)
            messages.success(request, "Guardado exitoso")
        else:
            messages.error(request, "No se pudo guardar")
        context = {
        "titulo13":'Formulario de Colas',
        "el_form13":form13,
        "num_rol13":num_rol13,
        "estados":pending
        }
        return render(request, "colas.html",context)
    else:
        
        return redirect('login')

def turnos(request):
    prioridad=Ticket.objects.filter(prioridad_id='1').filter(estado='6')
    prioridadMedia=Ticket.objects.filter(prioridad_id='2').filter(estado='6')
    prioridadbaja=Ticket.objects.filter(prioridad_id='3').filter(estado='6')
    print(settings.BASE_DIR)
    if prioridad.values():
        context = {

                "estados":prioridad,
                }
        return render(request, "turnos.html",context)
    elif prioridadMedia.values():
        context = {
                "estados":prioridadMedia,
                }
        return render(request, "turnos.html",context)
    elif prioridadbaja.values():
        context = {
                "estados":prioridadbaja
                }

        return render(request,"turnos.html",context)
    elif prioridadbaja:False
    return render(request,"ticket.html")


@login_required
def custom_logout(request):
    logout(request)
    messages.info(request, "Logged out successfully!")
    return redirect('login')

def custom_login(request):
    if request.user.is_authenticated:
        return redirect('login')

    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            user = authenticate(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
            )
            if user is not None:
                login(request, user)
                messages.success(request, f"Hello <b>{user.username}</b>! You have been logged in")
                return redirect('inicio')

        else:
            for error in list(form.errors.values()):
                messages.error(request, error) 

    form = AuthenticationForm() 
    
    return render(
        request=request,
        template_name="login.html", 
        context={'form': form}
        )
def editar(request, pk):
    nuevoestado = Ticket.objects.filter(ticket_id=pk)
    nuevoestado = Ticket.objects.get(estado='1')
    nuevoestado.save()
    return render(request, "turnos.html")


def guardarTurnoEditado(request):

        colaEspera = Ticket.objects.get(ticket_id=request.POST['id'])
        colaEspera.estado = Estado.objects.get(estado_id=request.POST['estado'])
        colaEspera.prioridad_id = Prioridades.objects.get(prioridad_id=request.POST['prioridad_id']) 
        colaEspera.puesto_id = Puesto.objects.get(puesto_id=request.POST['puesto_id']) 
        colaEspera.save()
    
    
        return redirect('inicio')

def editarTurno(request, id):
    try:
        colaEspera = Ticket.objects.get(ticket_id=id)
        estados = Estado.objects.all()
        prioridades = Prioridades.objects.all()
        puestos = Puesto.objects.all()
        listaEstados = []
        listaPrioridades = []
        listaPuestos = []
        if estados:
            for c in estados:
                dic = model_to_dict(c)
                listaEstados.append(dic)
        if prioridades:
            for c in prioridades:
                dic = model_to_dict(c)
                listaPrioridades.append(dic)
        if puestos:
            for c in puestos:
                dic = model_to_dict(c)
                listaPuestos.append(dic)
        return render(request, 'editarTurno.html',
                      {"colaEspera": colaEspera, "estados": listaEstados, "prioridades": listaPrioridades, "puestos":listaPuestos})
    except:
        return render(request, 'editarTurno.html',
                      {"colaEspera": colaEspera, "estados": listaEstados, "prioridades": listaPrioridades, "puestos":listaPuestos})


