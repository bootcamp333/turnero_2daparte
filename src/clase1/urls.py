"""clase1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from turnero import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('inicio/', views.Inicio, name='inicio'),
    path('personas/', views.Personavista, name='Personas'),
    path('roles/', views.roles, name='Roles'),
    path('clientes/', views.Clientevista, name='Clientes'),
    path('nacionalidad/', views.Nacionalidadvista, name='Nacionalidad'),
    path('tipoCliente/', views.TipoClientevista, name='Tipo-Clientes'),
    path('trabajador/', views.Trabajadorvista, name='Trabajadores'),
    path('puesto/', views.Puestovista, name='Puestos'),
    path('ciudad/', views.Ciudadvista, name='Ciudades'),
    path('estado/', views.Estadovista, name='Estados'),
    path('ticket/', views.Ticketvista, name='Tickets'),
    path('prioridad/', views.Prioridadvista, name='Prioridades'),
    path('discapacidad/', views.Discapacidadvista, name='Discapacidades'),
    path('servicios/', views.Serviciovista, name='Servicios'),
    path('colas/', views.Colavista, name='Cola_espera'),
    path('turnos/', views.turnos, name='Turnos'),
    path('login/', views.custom_login, name='login'),
    path('logout/', views.custom_logout, name='logout'),
    path('turnos/editarTurno/<int:id>/', views.editarTurno),
    path('guardarTurnoEditado/', views.guardarTurnoEditado),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
