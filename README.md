1- Instalar python y pip en la pc
2- PS \test\src> test\Scripts\activate
3- Activar el entorno virtual para usar django.
    windows 
        test\Scripts\activate
        
    UNIX
        source test/bin/activate

    Obs: para desactivar entorno virtual en se ejecuta deactivate desde cualquier lado
         Siempre que se vaya a trabajar en el proyecto se tiene que estar dentro del entorno virtual
4- ejecutar pip install django y pip install psycopg2 para usar django con postgresql
5- Si no se tiene instalado postgresql, instalar postgresql en el puerto especificado, con el
nombre de usuario host , base de datos y contraseñas respectivos.
6- Correr los migrations pyhton manage.py migrate
7- Crear super usuario python manage.py createsuperuser
8- Iniciar el proyecto haciendo python  manage.py runserver

